package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;


public class NotesActivity extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;
    private Button AddNewNote;
    static public ListView listView;
    public static ArrayList<String> notes= new ArrayList<>();
    static ArrayAdapter arrayAdapter;

    public static final String FILE_NAME="example.txt";

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.functions,menu);
        return true;


    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

int id=item.getItemId();
    if(id==R.id.item1) Logout();
    if(id==R.id.item2)
    {
        startActivity(new Intent(NotesActivity.this,Photo.class));
    }
    if(id==R.id.item4)
    {
        startActivity(new Intent(NotesActivity.this,asyncronJSON.class));

    }
    if(id==R.id.item5)
    {
        startActivity(new Intent(NotesActivity.this,Report.class));
    }
    if(id==R.id.item6)
    {
        Toast.makeText(this, "Progress Saved", Toast.LENGTH_SHORT).show();
        LogoutAndSave();
    }
    if(id==R.id.item7)
    {
        startActivity(new Intent(NotesActivity.this,location.class));
    }

return super.onOptionsItemSelected(item);



    }

    public void Logout()
    {
        firebaseAuth.signOut();
        finish();
        notes.clear();
        startActivity(new Intent(NotesActivity.this,MainActivity.class));
    }
    public void LogoutAndSave()
    {
        firebaseAuth.signOut();
        finish();
        startActivity(new Intent(NotesActivity.this,MainActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);
        firebaseAuth= FirebaseAuth.getInstance();



        NotesCreateAP();
        AddNewNote();
        DeleteNote();
    }
    protected void AddNewNote()
    {
        AddNewNote=(Button)findViewById(R.id.NAButton);
        AddNewNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(getApplicationContext(),NoteEditorActivity.class);
                startActivity(intent);
            }
        });
    }
    protected void DeleteNote()
    {
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id)
            {
                final int deleteIndex=position;
                new AlertDialog.Builder(NotesActivity.this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Are you sure?")
                        .setMessage("do you want to delete this Note?")
                        .setPositiveButton("yes", new DialogInterface.OnClickListener()
                                                                {
                                                                    @Override
                                                                    public void onClick(DialogInterface dialog, int which) {
                                                                        notes.remove(deleteIndex);
                                                                        arrayAdapter.notifyDataSetChanged();
                                                                    }

                                                                })
                                                                .setNegativeButton("No",null)
                                                                .show();

                return true;
            }
        });
    }

    protected void NotesCreateAP()
    {
        listView=(ListView)findViewById(R.id.NAListView);

        notes.add("New Note..");
        arrayAdapter =new ArrayAdapter(this,android.R.layout.simple_list_item_1,notes);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent= new Intent(getApplicationContext(),NoteEditorActivity.class);
                intent.putExtra("noteId",position);
                startActivity(intent);
            }
        });
    }

}
