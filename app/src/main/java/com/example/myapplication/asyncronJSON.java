package com.example.myapplication;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class asyncronJSON extends AppCompatActivity {

    Button b1;
    public static TextView data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asyncron_json);

        b1= findViewById(R.id.AsyncButton);
        data=(TextView) findViewById(R.id.AsyncTextView);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseJson PJ= new ParseJson();
                PJ.execute();


            }
        });
    }
}
