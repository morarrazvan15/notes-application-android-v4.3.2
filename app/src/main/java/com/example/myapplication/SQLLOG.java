package com.example.myapplication;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class SQLLOG extends AppCompatActivity {
    DatabaseHelp db;
    EditText e1,e2,e3;
    Button b1;
    Button b2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sqllog);
        db= new DatabaseHelp(this);
        e1=(EditText)findViewById(R.id.SQLEmail);
        e2=(EditText)findViewById(R.id.SQLPassword);
        b1=(Button)findViewById(R.id.SQLButton);
        b2=(Button)findViewById(R.id.SQLButtonLog);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s1=e1.getText().toString();
                String s2=e2.getText().toString();

                if(s1.equals("")||s2.equals("")){
                    Toast.makeText(getApplicationContext(),"Fields are empty", Toast.LENGTH_SHORT).show();

                }
                else{
                    Boolean checkEmail = db.checkEmail(s1);
                    if(checkEmail==true){
                        Boolean insert=db.insert(s1,s2);
                        if(insert==true){
                            Toast.makeText(getApplicationContext(),"Registered Succesfully",Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        Toast.makeText(getApplicationContext(),"Registration Failed. Email Exists",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email=e1.getText().toString();
                String password=e2.getText().toString();
                Boolean checkLog= db.CheckLogIn(email,password);
                if(checkLog==true)
                {
                    startActivity(new Intent(SQLLOG.this,NotesActivity.class));
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Wrong Email or Password",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}
