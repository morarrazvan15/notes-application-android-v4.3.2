package com.example.myapplication;



import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.widget.SearchView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import java.io.IOException;
import java.util.List;


public class location extends FragmentActivity implements OnMapReadyCallback {


    GoogleMap map;
    SupportMapFragment mapFragment;
    SearchView searchView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);


        searchView=findViewById(R.id.sv_location);
        mapFragment=(SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.google_map);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                String locationAdress= searchView.getQuery().toString();
                List<Address> adressList= null;

                if(locationAdress!=null || !locationAdress.equals(""))
                {

                    Geocoder geocoder= new Geocoder(location.this);
                    try{
                        adressList=geocoder.getFromLocationName(locationAdress,2);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if(adressList.isEmpty())
                    {
                    Toast.makeText(location.this,"Not a valid location",Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        for(int i=0;i<adressList.size();i++)
                        {
                            Address address= adressList.get(i);
                            LatLng latLng= new LatLng(address.getLatitude(),address.getLongitude());
                            map.addMarker(new MarkerOptions().position(latLng).title(locationAdress));
                            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,10));
                        }
                    }


                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

       mapFragment.getMapAsync(this);


    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        map=googleMap;

    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

    }


}
