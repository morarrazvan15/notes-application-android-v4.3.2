package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;

public class options extends AppCompatActivity {


    Button Notepad;
    Button NotePhoto;
    Button NoteLocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);


        Notepad= findViewById(R.id.NotePadJump);
        NotePhoto=findViewById(R.id.NotePhotoJump);
        NoteLocation=findViewById(R.id.NOTELOCATION);

        Notepad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(options.this,NotesActivity.class));
            }
        });

        NotePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(options.this,drawcanvas.class));
            }
        });

        NoteLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(options.this,location.class));
            }
        });
    }
}
