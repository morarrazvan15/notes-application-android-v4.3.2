package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


public class MainActivity extends AppCompatActivity {

    private EditText UserName;
    private EditText UserPassword;
    private Button Login;
    private TextView userRegistration;
    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressDialog;
    private TextView PasswordReset;
    private Button SQL;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;
    private CheckBox checkBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkBox=(CheckBox)findViewById(R.id.rememberMeCheckBox);
        UserName= (EditText)findViewById(R.id.MAUserName);
        UserPassword=(EditText)findViewById(R.id.MAPassword);
        Login=(Button)findViewById(R.id.MAButton);
        firebaseAuth=FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);
        PasswordReset= (TextView)findViewById(R.id.MAForgotPassword);
        FirebaseUser user= firebaseAuth.getCurrentUser();
        mPreferences=getSharedPreferences("com.example.myapplication", Context.MODE_PRIVATE);
        mEditor=mPreferences.edit();
        CheckSharedPref();

        SQL= (Button)findViewById(R.id.SQLJUMP);
        SQL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,SQLLOG.class));
            }
        });
        PasswordReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,ResetPasswordActivity.class));
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
            }
        });

        if(user!=null)
        {

        }
        userRegistration=(TextView)findViewById(R.id.MARegister);
        userRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,RegistrationActivity.class));
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
            }
        });


        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean checkData=true;

                    if(UserName.getText().toString().length()==0)
                    {
                                Toast.makeText(MainActivity.this,"Insert Name",Toast.LENGTH_SHORT).show();
                    }
                    else if(UserPassword.getText().toString().length()==0)
                    {
                        Toast.makeText(MainActivity.this,"Insert Password",Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        validate(UserName.getText().toString(),UserPassword.getText().toString());
                    }

                    if(checkBox.isChecked())
                    {
                        mEditor.putString(getString(R.string.checkBox),"True");
                        mEditor.commit();

                        String nm= UserName.getText().toString();
                        mEditor.putString(getString(R.string.UserName),nm);
                        mEditor.commit();

                        String ps=UserPassword.getText().toString();
                        mEditor.putString(getString(R.string.UserPassword),ps);
                        mEditor.commit();
                    }
                    else
                    {
                        mEditor.putString(getString(R.string.checkBox),"False");
                        mEditor.commit();


                        mEditor.putString(getString(R.string.UserName),"");
                        mEditor.commit();


                        mEditor.putString(getString(R.string.UserPassword),"");
                        mEditor.commit();
                    }


            }
        });





    }

    private void CheckSharedPref()
    {
String checkbox = mPreferences.getString(getString(R.string.checkBox),"False");
String password= mPreferences.getString(getString(R.string.UserPassword),"");
String name= mPreferences.getString(getString(R.string.UserName),"");

UserName.setText(name);
UserPassword.setText(password);
if(checkbox.equals("True"))
{
    checkBox.setChecked(true);
}
else
{
    checkBox.setChecked(false);
}
    }

    private void validate(String userName, String userPassword) {
        progressDialog.setMessage("Logging you in...");
        progressDialog.show();
        if(userName.length()==0) userName=" ";
        if(userPassword.length()==0) userPassword=" ";
        firebaseAuth.signInWithEmailAndPassword(userName,userPassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful())
                {
                    progressDialog.dismiss();
                    Toast.makeText(MainActivity.this,"Log In Succesfull",Toast.LENGTH_SHORT).show();
                    checkEmailVerification();
                    overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                    UserName.setText("");
                    UserPassword.setText("");
                }
                else
                {
                    progressDialog.dismiss();
                    Toast.makeText(MainActivity.this,"Log In Failed",Toast.LENGTH_SHORT).show();
                    UserName.setText("");
                    UserPassword.setText("");
                }
            }
        });

    }

private void checkEmailVerification()
{
    FirebaseUser firebaseUser= firebaseAuth.getInstance().getCurrentUser();
    Boolean emailCHECK= firebaseUser.isEmailVerified();

    if(emailCHECK)
    {
        finish();
        Toast.makeText(MainActivity.this,"Log In Succesfull",Toast.LENGTH_SHORT).show();
        startActivity(new Intent(MainActivity.this,options.class));
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);

    }
    else
    {
        Toast.makeText(this, "Verify your Email", Toast.LENGTH_SHORT).show();
        firebaseAuth.signOut();
    }
}





}
