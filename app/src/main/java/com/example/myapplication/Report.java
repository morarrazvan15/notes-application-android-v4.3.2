package com.example.myapplication;


import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Report extends AppCompatActivity {


    Button b1,b2;
    TextView e;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        b1=findViewById(R.id.SaveReportBtn);
        b2=findViewById(R.id.LoadReportBtn);
        e=findViewById(R.id.ReportTextView);
        e.setText("");
    }


    public void save(View view)
    {
        String text="";
        for(int i=0;i<NotesActivity.notes.size();i++)
            text=text+" "+ NotesActivity.notes.get(i);
        FileOutputStream fos= null;
        try {
            fos=openFileOutput(NotesActivity.FILE_NAME,MODE_PRIVATE);
            fos.write(text.getBytes());
            Toast.makeText(this,"SAVED TO"+getFilesDir()+"/"+NotesActivity.FILE_NAME,Toast.LENGTH_SHORT).show();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }finally {
            if(fos!=null) {
                try {
                    fos.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void load(View view)
    {
        FileInputStream fis=null;
        try {
            fis=openFileInput(NotesActivity.FILE_NAME);
            InputStreamReader isr=new InputStreamReader(fis);
            BufferedReader br= new BufferedReader(isr);
            StringBuilder sb= new StringBuilder();
            String text;
            while((text=br.readLine())!=null)
            {
            sb.append(text).append("\n");
            }
            e.setText(sb.toString());
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }finally {
            if(fis!=null) {
                try {
                    fis.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void loadCSV(View view)
    {

    startActivity(new Intent(Report.this,PieChartActivity.class));

    }

}
