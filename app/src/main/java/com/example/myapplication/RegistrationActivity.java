package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class RegistrationActivity extends AppCompatActivity {

    private EditText userName, userPassword, userEmail;
    private Button Register;
    private TextView userLogIn;
    private FirebaseAuth firebaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        setupUIViews();
        firebaseAuth=FirebaseAuth.getInstance();
        Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate())
                {
                    String user_Email= userEmail.getText().toString().trim();
                    String user_Password=userPassword.getText().toString().trim();
                    String user_Name=userName.getText().toString().trim();

                    firebaseAuth.createUserWithEmailAndPassword(user_Email,user_Password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful())
                            {
                                sendEmailVerification();

                            }
                            else
                            {
                                Toast.makeText(RegistrationActivity.this,"Registration fail",Toast.LENGTH_SHORT).show();
                            }

                        }
                    });
                }

            }
        });

        userLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegistrationActivity.this,MainActivity.class));
                overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
            }
        });


    }



    private void setupUIViews()
    {
        userName= (EditText) findViewById(R.id.raUserName);
        userPassword=(EditText) findViewById(R.id.raPassword);
        userEmail=(EditText)findViewById(R.id.raEmail);
        Register= (Button)findViewById(R.id.raButton);
        userLogIn= (TextView) findViewById(R.id.raAlreadyHaveAccount);

    }

    private Boolean validate()
    {
        Boolean result=false;
        String user_name= userName.getText().toString();
        String user_password= userPassword.getText().toString();
        String user_email= userEmail.getText().toString();
        if(user_name.isEmpty()|| user_password.isEmpty()|| user_email.isEmpty())
        {
            Toast.makeText(this,"Please Enter all the details",Toast.LENGTH_SHORT).show();
        }
        else

        {
            result = true;
        }
        return result;
    }
    private void sendEmailVerification()
    {
        final FirebaseUser firebaseUser=firebaseAuth.getCurrentUser();
        if(firebaseUser!=null)
        {
            firebaseUser.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {

                @Override
                public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful())
                {

                    Toast.makeText(RegistrationActivity.this,"Successfully Registered, Verification Sent",Toast.LENGTH_SHORT).show();
                    firebaseAuth.signOut();
                    finish();
                    startActivity(new Intent(RegistrationActivity.this,MainActivity.class));
                    overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
                }
                else
                {
                    Toast.makeText(RegistrationActivity.this,"Failed",Toast.LENGTH_SHORT).show();
                }
                }
            });
        }
    }








}
