package com.example.myapplication;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;


import com.github.mikephil.charting.charts.PieChart;

import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.util.ArrayList;
import java.util.List;

public class PieChartActivity extends AppCompatActivity {


    private static String TAG="CHART";
    private float[] DATASETN={15.3f,20.6f,64.26f,75.0f,41.81f,3.9f};
    private String[] DATASETS={"Andrei","Maria","George","Dan","Alex","Vanesa","Tom"};
    PieChart pieChart;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pie_chart);



        pieChart=(PieChart)findViewById(R.id.PieChartID);


        pieChart.setRotationEnabled(true);
        pieChart.setHoleRadius(25f);
        pieChart.setTransparentCircleAlpha(0);
        pieChart.setCenterText("Super Color Chart");
        pieChart.setDrawEntryLabels(true);

        addDataSet();

        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {

            }

            @Override
            public void onNothingSelected() {

            }
        });
    }

    private void addDataSet() {

        Log.d(TAG, "addDataSet: Started ");

        ArrayList<PieEntry> yEntryes= new ArrayList<>();
        ArrayList<String> xEntryes= new ArrayList<>();

        for(int i=0;i<DATASETN.length;i++)
        {
            yEntryes.add(new PieEntry(DATASETN[i],i));
        }

        for(int i=0;i<DATASETS.length;i++)
        {
            xEntryes.add(DATASETS[i]);
        }

        PieDataSet pieDataSet = new PieDataSet(yEntryes,"ASE 2020");
        pieDataSet.setSliceSpace(2);
        pieDataSet.setValueTextSize(12);

        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(Color.GRAY);
        colors.add(Color.BLUE);
        colors.add(Color.YELLOW);
        colors.add(Color.RED);
        colors.add(Color.GREEN);
        colors.add(Color.MAGENTA);

        pieDataSet.setColors(colors);
        Legend legend=pieChart.getLegend();
        legend.setForm(Legend.LegendForm.CIRCLE);

        PieData pieData = new PieData(pieDataSet);
        pieChart.setData(pieData);
        pieChart.invalidate();
    }


}
